class Course < ApplicationRecord
  validates_presence_of :name, :author, :state

  belongs_to :category
  after_create :send_email

  private

  def send_email
    CreateMailer.send_mail(self)
  end
end
