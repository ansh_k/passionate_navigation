class Category < ApplicationRecord
  validates_presence_of :name, :state
  validate :check_uniqness

  belongs_to :vertical
  has_many :categories

  after_create :send_email

  private

  def check_uniqness
    if Vertical.pluck(:name).include?(name) || Category.pluck(:name).include?(name)
      errors.add(:name, 'should be uniq across category and vertical both models')
    end
  end

  def send_email
    CreateMailer.send_mail(self)
  end
end
