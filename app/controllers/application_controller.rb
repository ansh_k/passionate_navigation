class ApplicationController < ActionController::API
  include AuthorizeRequest
  include ApiResponse
  include ExceptionHandler

  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  rescue_from ActionController::RoutingError, with: :render_404
  rescue_from AbstractController::ActionNotFound, with: :render_404

  before_action :check_authorization
end
