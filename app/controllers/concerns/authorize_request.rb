require 'active_support/concern'

module AuthorizeRequest
  extend ActiveSupport::Concern

  def check_authorization
    header = request.headers['X-Authorization']
    header = header.split(' ').last if header
    begin
      @decoded = JsonWebToken.decode(header)
    rescue ActiveRecord::RecordNotFound => e
      render json: { errors: e.message }, status: :unauthorized
    rescue JWT::DecodeError => e
      render json: { errors: e.message }, status: :unauthorized
    end
  end
end