require 'active_support/concern'

module ApiResponse
  extend ActiveSupport::Concern

  def api_response(status=:ok, success=true, errors=nil)
    unless errors.present?
      render json: {
        success: success
      }, status: status
    else
      render json: {
        success: success,
        errors: errors
      }, status: status
    end
  end
end
