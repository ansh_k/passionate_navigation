class Api::V1::CoursesController < ApplicationController
  before_action :load_course, only: %i[show update destroy]
  
  def index
    @courses = Course.all
    render json: @courses
  end

  def show
    render json: @course
  end

  def create
    @course = Course.new course_params
    if @course.save
      api_response(:created, true)
    else
      api_response(:unprocessable_entity, false, @course.errors.full_messages)
    end
  end

  def update
    if @course.update course_params
      api_response(:ok, true)
    else
      api_response(:unprocessable_entity, false, @course.errors.full_messages)
    end
  end

  def destroy
    @course.destroy
    api_response(:ok, true)
  end

  private

  def course_params
    params.require(:course).permit(:name)
  end

  def load_course
    @course = Course.find(params[:id])
  end
end
