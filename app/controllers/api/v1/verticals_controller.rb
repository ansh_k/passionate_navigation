class Api::V1::VerticalsController < ApplicationController
  before_action :load_vertical, only: %i[show update destroy]
  
  def index
    @verticals = Vertical.all
    render json: @verticals
  end

  def show
    render json: @vertical
  end

  def create
    @vertical = Vertical.new vertical_params

    if @vertical.save
      api_response(:created, true)
    else
      api_response(:unprocessable_entity, false, @vertical.errors.full_messages)
    end
  end

  def update
    if @vertical.update vertical_params
      api_response(:ok, true)
    else
      api_response(:unprocessable_entity, false, @vertical.errors.full_messages)
    end
  end

  def destroy
    @vertical.destroy
    api_response(:ok, true)
  end

  private

  def vertical_params
    params.require(:vertical).permit(:name)
  end

  def load_vertical
    @vertical = Vertical.find(params[:id])
  end
end
