class Api::V1::CategoriesController < ApplicationController
  before_action :load_category, only: %i[show update destroy]
  
  def index
    @categories = Category.all
    render json: @categories
  end

  def show
    render json: @category
  end

  def create
    @category = Category.new category_params
    if @category.save
      api_response(:created, true)
    else
      api_response(:unprocessable_entity, false, @category.errors.full_messages)
    end
  end

  def update
    if @category.update category_params
      api_response(:ok, true)
    else
      api_response(:unprocessable_entity, false, @category.errors.full_messages)
    end
  end

  def destroy
    @category.destroy
    api_response(:ok, true)
  end

  private

  def category_params
    params.require(:category).permit(:name)
  end

  def load_category
    @category = Category.find(params[:id])
  end
end
