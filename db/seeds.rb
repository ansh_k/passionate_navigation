
#Verticals

v1 = Vertical.create!(name: 'Health & Fitness')
v2 = Vertical.create!(name: 'Business')
v3 = Vertical.create!(name: 'Music')

#Categories

c1 = Category.create!(name: 'Booty & Abs', vertical_id: v1.id, state: 'active')
c2 = Category.create!(name: 'Full Body', vertical_id: v1.id, state: 'active')
c3 = Category.create!(name: 'Advertising', vertical_id: v2.id, state: 'active')
c4 = Category.create!(name: 'Writing', vertical_id: v2.id, state: 'active')
c5 = Category.create!(name: 'Singing', vertical_id: v3.id, state: 'active')
c6 = Category.create!(name: 'Music Fundamentals', vertical_id: v3.id, state: 'active')

#Courses

Course.create!(name: 'Loose the Gutt, keep the Butt', author: 'Anowa', category_id: c1.id, state: 'active')
Course.create!(name: 'BrittneBabe Fitness Transformation', author: 'Brittnebabe', category_id: c1.id, state: 'active')
Course.create!(name: 'BTX: Body Transformation Extreme', author: 'Barstarzz', category_id: c2.id, state: 'active')
Course.create!(name: 'Facebook Funnel Marketing', author: 'Russell Brunson', category_id: c2.id, state: 'active')
Course.create!(name: 'Build a Wild Audience', author: 'Tim Nilson', category_id: c3.id, state: 'active')
Course.create!(name: 'Editorial Writing Secrets', author: 'J. K. Rowling', category_id: c4.id, state: 'active')
Course.create!(name: 'Scientific Writing', author: 'Stephen Hawking', category_id: c4.id, state: 'active')
Course.create!(name: 'Vocal Training 101', author: 'Linkin Park', category_id: c5.id, state: 'active')
Course.create!(name: 'Music Production', author: 'Music Performance for Beginners', category_id: c5.id, state: 'active')
Course.create!(name: 'Learn the Piano', author: 'Lang Lang', category_id: c6.id, state: 'active')
Course.create!(name: 'Become a guitar hero', author: 'Jimmy Page', category_id: c6.id, state: 'active')
