class CreateCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :courses do |t|
      t.string :name
      t.string :author
      t.references :category, index: true
      t.string :state
      t.timestamps
    end
  end
end
