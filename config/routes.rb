Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :api do
    namespace :v1, defaults: { format: 'json' } do
      resources :verticals, except:  %i[new edit]
      resources :categories, except: %i[new edit]
      resources :courses, except: %i[new edit]
    end
  end
  match '*unmatched_route', :to => 'application#raise_not_found!', :via => :all
end
